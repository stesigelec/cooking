package main

import (
	"bytes"
	"html/template"
)

//Templates defines the templates set and methods to load and dynamically create templates
type Templates struct {
	*template.Template
}

//load all templates from templates folder
func (t *Templates) loadTemplates() {
	t.Template = template.New("root")
	t.Template.Funcs(
		template.FuncMap{
			"runtemplate": func(name string, o interface{}) (template.HTML, error) {
				return t.runTemplate(name, o)
			},
		},
	)
	t.Template = template.Must(t.ParseGlob("templates/*.tmpl.html"))
}

//custom method to execute templates dynamically
func (t *Templates) runTemplate(name string, o interface{}) (template.HTML, error) {
	var buf bytes.Buffer
	err := t.ExecuteTemplate(&buf, name, o)
	return (template.HTML(buf.String())), err
}
