// This file is part of cooking.

// cooking is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// cooking is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with cooking.  If not, see <http://www.gnu.org/licenses/>

package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

//templates are accessible from all handlers
//Templates struct is defined in templates.go file
//with methods to load and run dynamic templates
var templates Templates

//The head struct provides metadata for the page
type Head struct {
	Title string
	Tags  []string
}

//The page struct describes what is a webpage.
//Each page populates an instance of the struct with relevant values
type Page struct {
	Head       //embedded struct
	FooterText string
}

//homeHandler displays the home page
func homeHandler(w http.ResponseWriter, r *http.Request) {
	var p Page
	p.Title = "(structure generated) home page"
	p.Tags = append(p.Tags, "golang", "esigelec")
	p.FooterText = "(c)Some rights reserved"
	err := templates.ExecuteTemplate(w, "pageAccueil.tmpl.html", p)
	if err != nil {
		log.Panicln("home page cannot be displayed")
	}
}

//listRecipesHandler lists all the recipes
func listRecipesHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "here will all recipes be listed")
}

//displayRecipeHandler displays one selected recipe
func displayRecipeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "here will one recipe be displayed")
}

//addRecipeHandler display the add recipe form
func addRecipeHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "here will the recipe-adding form be displayed")
}

//main launches the webserver
func main() {
	//load and cache templates to global variable
	//runtime error if parsing fails
	templates.loadTemplates()
	//create muxer
	r := mux.NewRouter()
	//muxer rules
	r.HandleFunc("/recipes/add/", addRecipeHandler)
	r.HandleFunc("/recipes/view/{recipeId}", displayRecipeHandler)
	r.HandleFunc("/recipes/list", listRecipesHandler)
	r.HandleFunc("/recipes/add/", addRecipeHandler)
	r.HandleFunc("/", homeHandler)
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	//start server
	http.ListenAndServe(":8100", r)
}
